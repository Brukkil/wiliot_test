#!/bin/sh
SSH_in='22'
SSH_out='2021'

Apache_in='81'
Apache_out='9023'

Tomcat_in='8083'
Tomcat_out='9024'


printf "\033c"
echo 'SSH_in       \e[92m<\e[39m port:' "\e[32m$SSH_in" '\e[92m  |\e[39m'
echo 'SSH_out      \e[34m>\e[39m port:' "\e[34m$SSH_out" '\e[92m|'
echo '                          \e[92m|\e[39m'
echo 'Apache_in   \e[92m <\e[39m port:' "\e[32m$Apache_in" '\e[92m  |\e[39m'
echo 'Apache_out  \e[34m >\e[39m port:' "\e[34m$Apache_out" '\e[92m|'
echo '                          \e[92m|\e[39m'
echo 'Tomcat_in   \e[92m <\e[39m port:' "\e[32m$Tomcat_in" '\e[92m|\e[39m'
echo 'Tomcat_out  \e[34m >\e[39m port:' "\e[34m$Tomcat_out" '\e[92m|\e[94m'
echo '---------------------------\e[39m'

dir=$(pwd)
cid=$(docker build . | grep 'Successfully built' | awk 'NF>1{print $NF}')
touch .old_cid

old_cid=$(cat .old_cid)
docker images rm $($old_cid)
$cid > .old_cid
# docker rmi $($old_cid)
docker run -p $Apache_out:$Apache_in -p $Tomcat_out:$Tomcat_in -p $SSH_out:$SSH_in --rm -d --name ta -e CATALINA_HOME=/opt/tomcat $cid
