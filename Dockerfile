FROM debian
USER root
     # Download packages
RUN  apt update && apt upgrade -y; \
     apt install supervisor sudo nano ssh apache2 php libapache2-mod-php tar curl unzip sudo python3 wget default-jdk -y

     # Setup supervisor
RUN  mkdir -p /var/log/supervisor; \
     mkdir -p /etc/supervisor/conf.d; \
     mkdir /var/log/supervisord; \
     a2enmod cgi
     # Surepvisor conf
COPY config/supervisor.conf /etc/supervisor/conf.d/supervisord.conf
     # Apache conf
COPY config/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY config/apache2.conf /etc/apache2/apache2.conf
COPY config/ports.conf /etc/apache2/ports.conf
COPY data/public_html /var/www/html/

     # Add user FareOn
RUN  useradd -m -G sudo -s /bin/bash FareOn; \
     sudo mkdir -p /home/FareOn/.ssh; \
     touch /home/FareOn/.ssh/authorized_keys; \
     chown -R FareOn:FareOn /home/FareOn/.ssh/; \
     chmod -R 700 /home/FareOn/.ssh; \
     # Set password
     echo FareOn:fire | chpasswd
     # FareOn conf
COPY data/authorized_keys /home/FareOn/.ssh/authorized_keys
COPY config/sshd_conf /etc/ssh/sshd_config


     # install tomcat
RUN  mkdir -p /opt/tomcat; \
     groupadd tomcat; \
     useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat; \
     cd /tmp; \
     curl -O https://www2.apache.paket.ua/tomcat/tomcat-9/v9.0.45/bin/apache-tomcat-9.0.45.zip; \
     unzip apache-tomcat-9.0.45.zip -d /opt/tomcat; \
     mv /opt/tomcat/apache-tomcat-9.0.45/* /opt/tomcat; \
     cd /opt/tomcat; \
     chgrp -R tomcat /opt/tomcat; \
     chmod -R g+r conf; \
     chmod g+x conf; \
     cd /opt/tomcat; \
     chgrp -R tomcat /opt/tomcat; \
     chmod -R g+r conf; \
     chmod g+x conf; \
     chown -R tomcat webapps/ work/ temp/ logs/; \
     sed -i 's/8080/8083/' /opt/tomcat/conf/server.xml; \
     curl https://raw.githubusercontent.com/apache/tomcat/master/bin/catalina.sh > /opt/tomcat/catalina.sh


     # Tomcat conf
COPY config/tomcat.service /etc/systemd/system/tomcat.service
COPY config/tomcat-users.xml /opt/tomcat/conf/tomcat-users.xml


CMD ["/usr/bin/supervisord"]
EXPOSE 2021
EXPOSE 2023
EXPOSE 9024
